import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MockitoTest {
  @Mock
  IBitwise bitwise;
  @InjectMocks
  AdvancedCalculator advancedCalculator = new AdvancedCalculator();

  @BeforeEach
  void init(){
    System.out.println("Before");
  }

  @AfterEach
  void exit(){
    System.out.println("After");
  }

  @Test
  void testAddAndBitwiseOr(){
    lenient().when(bitwise.bitwiseOR(12,25)).thenReturn(29);
    lenient().when(bitwise.bitwiseOR(10,12)).thenReturn(14);
    assertEquals(29,advancedCalculator.addAndBitwiseOR(10,2,25));
    assertEquals(14,advancedCalculator.addAndBitwiseOR(5,5,12));
  }

  @Test
  void testAddAndBitwiseAnd(){
    lenient().when(bitwise.bitwiseAND(12,25)).thenReturn(8);
    lenient().when(bitwise.bitwiseAND(10,20)).thenReturn(0);
    assertEquals(8,advancedCalculator.addAndBitwiseAnd(10,2,25));
    assertEquals(0,advancedCalculator.addAndBitwiseAnd(5,5,20));
  }
}
