import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.*;

public class BitwiseCalculatorTest {
    private BitwiseCalculator calculator;

    @BeforeEach
    public void init(){
      calculator = new BitwiseCalculator();
      System.out.println("Before");
    }

    @AfterEach
    public void exit(){
      calculator = null;
      System.out.println("After");
    }

    @Test
    void testBitwiseOr(){
       assertEquals(29,calculator.bitwiseOR(12,25));
    }

    @Test
    void testBitwiseAnd(){
      assertTrue(calculator.bitwiseAND(12,25) == 8);
    }

    @Test
    void testBitwiseRightShift(){
      assertEquals(106,calculator.bitwiseRightShift(212,1));
    }

    @Test
    void testVoidSetter(){
      calculator.setNum(4);
      assertEquals(4,calculator.getNum());
    }

    @Test
    @Disabled
    void disabledTest(){
      assertEquals(4,calculator.bitwiseOR(4,2));
    }
}
