public class BitwiseCalculator implements IBitwise {

  int num;
  BitwiseCalculator(){}
  @Override
  public int bitwiseOR(int a, int b) {
    return (a | b);
  }

  @Override
  public int bitwiseAND(int a, int b) {
    return (a & b);
  }

  @Override
  public int bitwiseRightShift(int a, int b) {
    return (a >> b);
  }

  public void setNum(int num) {
    this.num = num;
  }

  public int getNum() {
    return num;
  }
}
