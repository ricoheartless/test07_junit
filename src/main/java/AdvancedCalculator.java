public class AdvancedCalculator {
  private BitwiseCalculator calculator;

  AdvancedCalculator(){
    calculator = new BitwiseCalculator();
  }

  public int addAndBitwiseOR(int a, int b, int c){
    return calculator.bitwiseOR(a + b,c);
  }

  public int addAndBitwiseAnd(int a, int b, int c){
    return calculator.bitwiseAND(a+b,c);
  }

}
