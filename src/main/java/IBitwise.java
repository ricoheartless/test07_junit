public interface IBitwise {
  int bitwiseOR(int a, int b);
  int bitwiseAND(int a, int b);
  int bitwiseRightShift(int a, int b);
}
